package main

import (
	"encoding/base64"
	"fmt"
	"github.com/godbus/dbus/v5"
	"luksbackup/utils/confparse"
	"os"
)

func loadConf(confFn string) confparse.Conf {
	var rawConf, err = os.ReadFile(confFn)
	if err != nil {
		fmt.Errorf("Could not read configuration file at %s: %v", confFn, err)
	}
	var _confFn = "/home/nova/projects/go/gitlab.com/novamacintyre/luksbackup/testdata/util/confparse/valid_conf"
	var conf, nErr, eErr, ok = confparse.ParseConf(rawConf)
	if !ok {
		fmt.Errorf("Could not parse configuration file at %s: %v, %v", _confFn, nErr, eErr)
	}
	return conf
}

func capturePassword(passwords chan [2]string) {
	fmt.Println("Hello?")
	conn, err := dbus.ConnectSessionBus()
	if err != nil {
		fmt.Fprintln(os.Stderr, "Failed to connect to session bus:", err)
		os.Exit(1)
	}
	defer conn.Close()

	rules := []string{
		"type='signal',member='MountOpReply2',path='/org/gtk/Private/RemoteVolumeMonitor',interface='org.gtk.Private.RemoteVolumeMonitor'",
		"type='method_call',member='MountOpReply2',path='/org/gtk/Private/RemoteVolumeMonitor',interface='org.gtk.Private.RemoteVolumeMonitor'",
		"type='method_return',member='MountOpReply2',path='/org/gtk/Private/RemoteVolumeMonitor',interface='org.gtk.Private.RemoteVolumeMonitor'",
		"type='error',member='MountOpReply2',path='/org/gtk/Private/RemoteVolumeMonitor',interface='org.gtk.Private.RemoteVolumeMonitor'",
	}
	var flag uint = 0

	call := conn.BusObject().Call("org.freedesktop.DBus.Monitoring.BecomeMonitor", 0, rules, flag)
	if call.Err != nil {
		fmt.Fprintln(os.Stderr, "Failed to become monitor:", call.Err)
		os.Exit(1)
	}

	c := make(chan *dbus.Message, 10)
	conn.Eavesdrop(c)
	fmt.Println("Monitoring notifications")
	for v := range c {
		fmt.Println(v)
		if len(v.Body) != 9 {
			continue
		}
		arrayDecodedPassword, _ := base64.StdEncoding.DecodeString(v.Body[4].(string))
		decodedPassword := string(arrayDecodedPassword)

		fmt.Println(decodedPassword)
		passwords <- [2]string{decodedPassword, ""}
	}
}

func isDeviceMounted(devString string) bool {
	// Might not need this
	return false
}

func getAliasedDevices() {

}

func unlockDevice() {
	// A specific unlock function is probably unnecessary...
}

func storeSecret() {

}

func getSecret(conf confparse.Conf, devStringChan *chan string, secrets chan map[string]string) {
	conn, err := dbus.ConnectSessionBus()
	if err != nil {
		fmt.Errorf("failed to connect to session bus: %v", err)
	}
	defer conn.Close()

	obj := conn.Object("org.freedesktop.secrets", "/org/freedesktop/secrets")
	// TODO: Use DH during session bus instantiation
	session := obj.Call(
		"org.freedesktop.Secret.Service.OpenSession",
		0,
		"plain", dbus.MakeVariant(""))
	sessionPath := session.Body[1].(dbus.ObjectPath)

	for devString := range *devStringChan {
		args := map[string]string{"gvfs-luks-uuid": devString}
		call := obj.Call("org.freedesktop.Secret.Service.SearchItems", 0, args)
		item := call.Body[0].([]dbus.ObjectPath)
		if len(item) == 0 {
			continue
		}
		call = obj.Call("org.freedesktop.Secret.Service.GetSecrets", 0, item, sessionPath)
		secret := call.Body[0].(map[dbus.ObjectPath][]interface{})[item[0]][2].(string)
		secrets <- map[string]string{devString: secret}
	}
}

// monitorDeviceConnection detects LUKS volumes attaching to the system
func monitorDeviceConnection(devStringChan *chan string) {
	conn, err := dbus.ConnectSessionBus()
	if err != nil {
		fmt.Errorf("failed to connect to session bus: %v", err)
	}
	defer conn.Close()

	rules := []string{
		"type='signal',member='VolumeAdded',path='/org/gtk/Private/RemoteVolumeMonitor',interface='org.gtk.Private.RemoteVolumeMonitor'",
		"type='method_call',member='VolumeAdded',path='/org/gtk/Private/RemoteVolumeMonitor',interface='org.gtk.Private.RemoteVolumeMonitor'",
		"type='method_return',member='VolumeAdded',path='/org/gtk/Private/RemoteVolumeMonitor',interface='org.gtk.Private.RemoteVolumeMonitor'",
		"type='error',member='VolumeAdded',path='/org/gtk/Private/RemoteVolumeMonitor',interface='org.gtk.Private.RemoteVolumeMonitor'",
	}
	var flag uint = 0

	call := conn.BusObject().Call("org.freedesktop.DBus.Monitoring.BecomeMonitor", 0, rules, flag)
	if call.Err != nil {
		fmt.Fprintln(os.Stderr, "Failed to become monitor:", call.Err)
		os.Exit(1)
	}

	c := make(chan *dbus.Message, 10)
	conn.Eavesdrop(c)
	fmt.Println("Monitoring notifications")
	for v := range c {
		fmt.Println(v)
		if len(v.Body) < 3 || len(v.Body[2].([]interface{})) < 5 {
			continue
		}
		ds := v.Body[2].([]interface{})[4].(string)
		*devStringChan <- ds
		fmt.Println(devStringChan)
	}
}

func main() {
	var passwords chan [2]string
	conf := loadConf("")
	devString := make(chan string, 5)
	var secrets chan map[string]string
	go capturePassword(passwords)
	go monitorDeviceConnection(&devString)
	go getSecret(conf, &devString, secrets)
	for p := range secrets {
		fmt.Println("Password: %s", p)
	}
}
