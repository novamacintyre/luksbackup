// Test confparse
// (C) Nova MacIntyre 2022

package confparse

import (
	"fmt"
	"os"
	"path/filepath"
	"testing"
)

func TestNewDevice(t *testing.T) {
	testDeviceStrings := map[string]bool{
		"deadbeef-0123-4567-89ab-cdef01234567": true,
		"fakeuuid-0123-4567-89ab-cdef01234567": false,
		"0123456789abcdef":                     false,
		"0123-abcd":                            false,
	}
	for devString, shouldPass := range testDeviceStrings {
		dev, err := NewDevice(devString)
		if (err == nil) != shouldPass {
			if shouldPass {
				t.Fatalf(`NewDevice(%s) failed with error %v`, devString, err)
			} else {
				t.Fatalf(`NewDevice(%s) succeeded with bad Device UUID: %v`, devString, err)
			}
		}
		if dev.uuid.String() != devString && shouldPass {
			t.Fatalf(`NewDevice(%s).uuid.String() == %s`, devString, dev.uuid.String())
		}
	}
}

func TestNewNode(t *testing.T) {
	testNodeStrings := map[string]bool{
		"deadbeef-0123-4567-89ab-cdef01234567:/path/to/dir/": true,
		"fakeuuid-0123-4567-89ab-cdef01234567:/path/to/dir/": false,
	}
	for nodeString, shouldPass := range testNodeStrings {
		n, err := NewNode(nodeString)
		if (err == nil) != shouldPass {
			if shouldPass {
				t.Fatalf(`NewNode(%s) failed with error %v`, nodeString, err)
			} else {
				t.Fatalf(`NewNode(%s) succeeded with bad Node string`, nodeString)
			}
		}
		if err == nil && shouldPass {
			if n.Path != filepath.Clean("/path/to/dir/") {
				t.Fatalf(`NewNode(%s) has incorrect path %s`, nodeString, n.Path)
			}
		}
	}
}

func TestNewEdge(t *testing.T) {
	testEdgeStrings := map[string]bool{
		"deadbeef-0123-4567-89ab-cdef01234567:/srcDir->54311357-0123-4567-89ab-cdef01234567:/dstDir":   true,
		"fakeuuid-0123-4567-89ab-cdef01234567:/srcDir->54311357-0123-4567-89ab-cdef01234567:/dstDir":   false,
		"deafbeef-0123-4567-89ab-cdef01234567:/srcDir->fakeuuid-0123-4567-89ab-cdef01234567:/dstDir":   false,
		"deafbeef-0123-4567-89ab-cdef01234567:/src->Dirbada6602-0123-4567-89ab-cdef01234567:/dstDir":   false,
		"badsep::-0123-4567-89ab-cdef01234567:/srcDir::54311357-0123-4567-89ab-cdef01234567:/dstDir":   false,
		"toomanys-ep23-4567-89ab-cdef01234567:/srcDir->->54311357-0123-4567-89ab-cdef01234567:/dstDir": false,
	}
	for edgeString, shouldPass := range testEdgeStrings {
		_, err := NewEdge(edgeString)
		if (err == nil) != shouldPass {
			if shouldPass {
				t.Fatalf(`NewEdge(%s) failed with error %v`, edgeString, err)
			} else {
				t.Fatalf(`NewEdge(%s) succeeded with bad Edge string`, edgeString)
			}
		}
	}
}

func TestIsNodeSubset(t *testing.T) {
	var failedNodes []string
	nodes := make(map[Node]bool)
	parentNode, _ := NewNode("deadbeef-0123-4567-89ab-cdef01234567:/dir")
	nodeStrings := map[string]bool{
		"deadbeef-0123-4567-89ab-cdef01234567:/dir/subdir":                true,
		"deadbeef-0123-4567-89ab-cdef01234567:/notsubdir":                 false,
		"deadbeef-0123-4567-89ab-cdef01234567:/dir_but_not_subdir":        false,
		"54311357-0123-4567-89ab-cdef01234567:/dir/notsubdir_diff-Device": false,
	}
	for nodeString, isSubset := range nodeStrings {
		n, err := NewNode(nodeString)
		if err != nil {
			// Then log and add to failedNodes so we can still fail, even if other nodes pass
			failedNodes = append(failedNodes, nodeString)
			t.Logf("NewNode(%s) failed to return Node: %v", nodeString, err)
			continue
		}
		nodes[n] = isSubset
	}
	for n, isSubset := range nodes {
		res := isNodeSubset(parentNode, n)
		if res != isSubset {
			if isSubset {
				t.Fatalf("%v should be subset of %v, but isNodeSubset says it is not", n, parentNode)
			} else {
				t.Fatalf("%v should not be subset of %v, but isNodeSubset says it is", n, parentNode)
			}
		}
	}
	if len(failedNodes) > 0 {
		t.Fatalf("Some nodes failed to initialise")
	}
}

func TestGetSrcNodes(t *testing.T) {
	t.Fatalf("Test case not implemented")
}

func TestIsNodeDeviceDeclared(t *testing.T) {
	t.Fatalf("Test case not implemented")
}

func TestIsNodeInjective(t *testing.T) {
	t.Fatalf("Test case not implemented")
}

func TestIsEdgeValid(t *testing.T) {
	t.Fatalf("Test case not implemented")
}

func TestValidateConf(t *testing.T) {
	t.Fatalf("Test case not implemented")
}

func TestParseConf(t *testing.T) {
	data, err := os.ReadFile("../testdata/util/confparse/valid_conf")
	if err != nil {
		t.Fatalf("Could not find test data: %s", err)
	}
	_, invalidNodes, invalidEdges, ok := ParseConf(data)
	if !ok {
		fmt.Printf("%v\n", len(invalidNodes))
		t.Fatalf("Parsing of valid configuration file failed: %v, %v", invalidNodes, invalidEdges)
	}
}
